
import os
import datetime
import sys
from pathlib import Path
from shutil import copyfile

targetDirectory = "./Learning Data/Characters-NoDuplicates/"
targetDataCount = 500
outputDirectory = ""



def CreateFolders():
    date = datetime.datetime.now()
    dirname = "./Learning Data/Training_"+date.strftime("%Y_%m_%d.%H_%M_%S" )
    os.mkdir( dirname )
    return dirname

targetDir = CreateFolders()

p = Path(targetDirectory)
for x in p.iterdir():
    if x.is_dir():
        newDirname = targetDir+"/"+x.name+"/"
        os.mkdir(newDirname)
        copyCount = 0
        while copyCount < targetDataCount:
            for y in x.iterdir():
                if( y.name.find(".ini") != -1): continue
                copyCount+=1
                copyfile( y, newDirname+str(copyCount)+".png" )

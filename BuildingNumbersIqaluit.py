import NumberFinderWithML
import MLTraining
import MapFitter
import NumberIdentifier
import FolderManager
import os
import sys
from zipfile import ZipFile

##    Starting point for Iqaluit Map Number project
##      -use build_published() for building out the location CSVs 
##          with fixed data.
##      -use build_named("TestBuild") to build to a specific Builds directory. 
##          can be used to pickup execution at a given point
##      -use build_timestamped("Builds/") to start a fresh build with a new 
##          timestamped directory

SAVE_DATA = True

LEARNING_DATA_ZIP = "Learning Data.zip"

mapInfo_small = ( "SmallTest", "NumbersSmall.png", ([[64,106],[ 63.729323, -68.445227]], [[398,246],[ 63.728715, -68.441922]] ))
mapInfo_apex = ( "Apex", "BuildingNumbersApex.png", ([[992,458],[ 63.732225, -68.453270]], [[2096, 1227],[ 63.728655, -68.441860]]) )
mapInfo_iqaluit = ( "Iqaluit", "BuildingNumbersIqaluit.png", ([[568,660],[ 63.761637, -68.544424]], [[7076, 6530],[ 63.734611, -68.477873]]) )


def build_named(dirName):
    #folderManager = FolderManager.create_timestamp_folder("./Builds/")
    folderManager = FolderManager.create_folder("Builds/"+dirName)
    build(folderManager)

def build_published():
    folderManager = FolderManager.create_folder("Published/")
    mlModel = get_ml_model(folderManager)
    parse_map(folderManager, mapInfo_apex, mlModel)
    parse_map(folderManager, mapInfo_iqaluit, mlModel)

def build_timestamped(root):
    folderManager = FolderManager.create_timestamp_folder(root)
    build(folderManager)

def build(folderManager):
    mlModel = get_ml_model(folderManager)
    parse_map(folderManager, mapInfo_apex, mlModel)
    parse_map(folderManager, mapInfo_iqaluit, mlModel)

def train_ml_model(folderManager):
    path = folderManager.GetPath( "Learning Data" )
    if( not os.path.exists(path) ):
        extract_learning_data( "Learning Data.zip", folderManager)
    return MLTraining.trainData26(path)


def parse_map( folderManager, mapInfo, mlModel ):
    print("Parsing Map "+mapInfo[1])
    mapFolderManager = FolderManager.create_folder(folderManager.GetPath("Map_"+mapInfo[0]+"/")) 
    groupInfo =  NumberFinderWithML.process_map_numbers( mapInfo[1], mapFolderManager, mlModel)
    MapFitter.export_numbers(mapInfo, mapFolderManager, groupInfo)

def parse_map_published( folderManager, mapInfo, mlModel ):
    print("Parsing Map "+ mapInfo[1])
    mapFolderManager = FolderManager.create_folder(folderManager.GetPath("Map_"+mapInfo[0]+"/")) 
    groupInfo =  NumberFinderWithML.get_super_groups_iqaluit( mapInfo[1], mapFolderManager, mlModel)
    MapFitter.export_numbers(mapInfo, mapFolderManager, groupInfo)

def compute_map_numbers(folderManager):
    mlModel = get_ml_model(folderManager)
    parse_map(folderManager, mapInfo_apex, mlModel)
    parse_map_published(folderManager, mapInfo_iqaluit, mlModel)  

def get_ml_model(folderManager):
    folderManager.CreateFolder("Data")
    path = folderManager.GetPath("Data/num_model.h5")
    model = None
    if( not os.path.exists(path) ):
        # We need to create the model
        model = train_ml_model(folderManager)
        model.save(path)
        return model
    else:
        return NumberIdentifier.load_model(path)

def extract_learning_data(zipFile, folderManager):
    # call extract_learning_data( "Learning Data.zip", folderManager)
    print( "Extracting Learning Data.")
    if( not os.path.exists( LEARNING_DATA_ZIP )):
        sys.exit("Unable to locate "+zipFile+". Aborting.")
    with ZipFile(zipFile, 'r') as zipObj:
        # Extract all the contents of zip file in different directory
        zipObj.extractall(folderManager.GetPath(""))

build_published()
#build_named("TestBuild")
#build_timestamped("Builds/")

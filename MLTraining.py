import os
import tensorflow as tf

DO_PREDICTIONS = False

def trainData11():
        model = tf.keras.models.Sequential([tf.keras.layers.Flatten(), 
                                        tf.keras.layers.Dense(256, activation=tf.nn.relu), 
                                        tf.keras.layers.Dense(11, activation=tf.nn.softmax)])

        model.compile(optimizer = 'adam',
                loss = 'sparse_categorical_crossentropy',
                metrics=['accuracy'])

        from tensorflow.keras.preprocessing.image import ImageDataGenerator
        
        # All images will be rescaled by 1./255
        train_datagen = ImageDataGenerator(rescale=1./255)
        
        # Flow training images in batches of 128 using train_datagen generator
        train_generator = train_datagen.flow_from_directory(
                './Learning Data/Training_10/',  # This is the source directory for training images
                target_size=(16, 16),  # All images will be resized to 150x150
                batch_size=128,
                # Since we use binary_crossentropy loss, we need binary labels
                class_mode='sparse')

        #print( train_generator[0])

        history = model.fit_generator(
        train_generator,
        epochs=5,
        verbose=1)

        test_datagen = ImageDataGenerator(rescale=1./255)
        test_generator = train_datagen.flow_from_directory(
                './Learning Data/Test/',  # This is the source directory for training images
                target_size=(16, 16),  # All images will be resized to 150x150
                batch_size=128,
                # Since we use binary_crossentropy loss, we need binary labels
                class_mode='sparse')

        model.evaluate(test_generator)

        return model

def trainData23():
        model = tf.keras.models.Sequential([tf.keras.layers.Flatten(), 
                                        tf.keras.layers.Dense(512, activation=tf.nn.relu),
                                        tf.keras.layers.Dense(23, activation=tf.nn.softmax)])

        model.compile(optimizer = 'adam',
                loss = 'sparse_categorical_crossentropy',
                metrics=['accuracy'])

        from tensorflow.keras.preprocessing.image import ImageDataGenerator
        
        # All images will be rescaled by 1./255
        train_datagen = ImageDataGenerator(rescale=1./255,)
        
        # Flow training images in batches of 128 using train_datagen generator
        train_generator = train_datagen.flow_from_directory(
                './Learning Data/Training_23/',  # This is the source directory for training images
                target_size=(16, 16),  # All images will be resized to 150x150
                batch_size=128,
                color_mode='grayscale',
                # Since we use binary_crossentropy loss, we need binary labels
                class_mode='sparse')

        model.fit_generator(
        train_generator,
        epochs=5,
        verbose=1)

        test_datagen = ImageDataGenerator(rescale=1./255)
        test_generator = train_datagen.flow_from_directory(
                './Learning Data/Test/',  # This is the source directory for training images
                target_size=(16, 16),  # All images will be resized to 150x150
                batch_size=128,
                color_mode='grayscale',
                # Since we use binary_crossentropy loss, we need binary labels
                class_mode='sparse')

        model.evaluate(test_generator)

        if( DO_PREDICTIONS ):
                predictions = model.predict(test_generator)
                for line in predictions:
                        print( line)
                
        return model

def trainData26(trainingDataPath):
        model = tf.keras.models.Sequential([tf.keras.layers.Flatten(), 
                                        tf.keras.layers.Dense(512, activation=tf.nn.relu),
                                        tf.keras.layers.Dense(128, activation=tf.nn.relu),
                                        tf.keras.layers.Dense(26, activation=tf.nn.softmax)])

        model.compile(optimizer = 'adam',
                loss = 'sparse_categorical_crossentropy',
                metrics=['accuracy'])

        from tensorflow.keras.preprocessing.image import ImageDataGenerator
        
        # All images will be rescaled by 1./255
        train_datagen = ImageDataGenerator(rescale=1./255,)
        
        # Flow training images in batches of 128 using train_datagen generator
        train_generator = train_datagen.flow_from_directory(
                trainingDataPath + '/Training_26/',  # This is the source directory for training images
                target_size=(16, 16),  # All images will be resized to 150x150
                batch_size=256,
                color_mode='grayscale',
                # Since we use binary_crossentropy loss, we need binary labels
                class_mode='sparse')
        
        model.fit_generator(
        train_generator,
        epochs=5,
        verbose=1)

        test_datagen = ImageDataGenerator(rescale=1./255)
        test_generator = test_datagen.flow_from_directory(
                './Learning Data/Test/',  # This is the source directory for training images
                target_size=(16, 16),  # All images will be resized to 150x150
                batch_size=64,
                color_mode='grayscale',
                # Since we use binary_crossentropy loss, we need binary labels
                class_mode='sparse')

        model.evaluate(test_generator)

        if( DO_PREDICTIONS ):
                predictions = model.predict(test_generator)
                #file = open( outDir + 'ModelFiles/num_model_3_predictions.txt','w') 
                for line in predictions:
                        print( line)
                        #file.write(str(line))
                #file.close()        

        return model
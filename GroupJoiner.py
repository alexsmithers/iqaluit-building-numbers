import ImageParseHelpers as Helpers
import NumberIdentifier

LOWER_NEIGHBOR_DIST = 15

LR_NEIGHBOR_Y_DIST = 10
LR_NEIGHBOR_X_DIST = 10

DEBUG = False

def GetGroupCenter(group):
    return ( group[1][0] * .5 + group[1][2] * .5, group[1][1] * .5 + group[1][3] * .5  )

def DoGroupsOverlapX(group1, group2):
    return ( (group1[1][0] >= group2[1][0] and group1[1][0] <= group2[1][2]) or
        (group1[1][2] >= group2[1][0] and group1[1][2] <= group2[1][2]) or
        (group1[1][0] <= group2[1][0] and group1[1][2] >= group2[1][2]) )

def IsNumber( num ):
    return num <10

def IsAllNumbers(group):
    for digit in group:
        if( not(IsNumber(digit))):
            return False
    return True

def FindUpperNeighbor(groups, index, dist = LOWER_NEIGHBOR_DIST):
    # We know that The lower neighbor must be of a higher index.
    group = groups[index]
    for i in range( index ):
        compGroup = groups[index - 1 - i]
        if( group[1][1] - compGroup[1][3] > dist):
            return -1
        if( DoGroupsOverlapX(group, compGroup)):
            return index - 1 - i
    return -1

def FindLowerNeighbor(groups, index, dist = LOWER_NEIGHBOR_DIST):
    # We know that The lower neighbor must be of a higher index.
    group = groups[index]
    for i in range( index+1, len(groups)):
        compGroup = groups[i]
        if( compGroup[1][1] - group[1][3] > dist):
            return -1
        if( DoGroupsOverlapX(group, compGroup)):
            return i
        
#returns -1 if test group is left neighbor, 1 if right neighbor, 0 if neither
def IsHorizontalNeighbor(centerGroup, testGroup):
    if( abs( GetGroupCenter(testGroup)[1] - GetGroupCenter(centerGroup)[1] ) < LR_NEIGHBOR_Y_DIST ):
        if( testGroup[1][0] - centerGroup[1][2] < LR_NEIGHBOR_X_DIST and testGroup[1][0] - centerGroup[1][2] >= 0):
            return 1
        elif(centerGroup[1][0] - testGroup[1][2] < LR_NEIGHBOR_X_DIST and centerGroup[1][0] - testGroup[1][2] >=0 ):
            return -1
    return 0

def FindRLNeighborsDir( groups, index, foundRL, dir):
    testIndex = index + dir
    while((foundRL[0] == -1 or foundRL[1] == -1) and testIndex >= 0 and testIndex < len(groups)
        and groups[min(index, testIndex)][1][3] >= groups[max(index, testIndex)][1][1]):
        horizNeighbor = IsHorizontalNeighbor(groups[index], groups[testIndex])
        if( horizNeighbor == -1):
            foundRL[0] = testIndex
        elif( horizNeighbor == 1):
            foundRL[1] = testIndex
        testIndex += dir
    return foundRL

def FindLRNeighbors(groups, index):
    foundRL = [-1, -1]
    foundRL = FindRLNeighborsDir( groups, index, foundRL, 1)
    foundRL = FindRLNeighborsDir(groups, index, foundRL, -1)
    return foundRL

def SplitBoxHorizontal(box):
    midpoint = box[0] + int( (box[2]-box[0] )/ 2 )
    return [(box[0], box[1], midpoint, box[3]),(midpoint, box[1], box[2], box[3]) ]

def LettersInRangeToString(ltr1, ltr2):
    outStr = NumberIdentifier.GetChar(ltr1)
    for num in range(ltr1+1, ltr2+1 ):
        if( num < 16 or num > 24):
            outStr+= ","+NumberIdentifier.GetChar(num)
    return outStr

def GetNumRangeString(num1, num2):
    outStr = str(num1)
    for num in range(num1+1, num2+1):
        outStr+=","+str(num)
    return outStr

def PadBox(box, padding):
    return (box[0]-padding, box[1]-padding, box[2]+padding, box[3]+padding)

def SuperGroupNumbers(groups):
    superGroups = []

    for i in range(len(groups)):
        group = groups[i]
        if(group[0][len(group[0])-1] == 23):
            found = FindLowerNeighbor(groups, i)
            if( found >= 0):
                groups[i].append(True)
                groups[found].append(True)
                if (DEBUG):
                    print("Found range numbers "+str(group)+" and "+str(groups[found]))
                startNum = Helpers.NumListToInt(group[0][:len(group[0])-1])
                endNum = Helpers.NumListToInt( groups[found][0] )
                combinedBox = Helpers.CombineBoxes(group[1], groups[found][1])
                for num in range( startNum, endNum + 1):
                    superGroups.append( [str(num), combinedBox ] )
        elif(len(group[0]) > 6 and 22 in group[0]  ):
            # num slash num.. break in half
            groups[i].append(True)
            str1 = []
            str2 = []
            found = False
            for index in range(len( group[0])):
                if( group[0][index] == 22 ):
                    found = True
                    continue
                if( found ):
                    str2.append(group[0][index])
                else:
                    str1.append(group[0][index])
            boxes = SplitBoxHorizontal(group[1] )
            superGroups.append( [Helpers.NumListToInt(str1), boxes[0]] )
            superGroups.append( [Helpers.NumListToInt(str2), boxes[1]] )
            if (DEBUG):
                print("Found pair numbers "+str(str1)+" and "+str(str2))
        elif(len(group[0]) == 8 and IsAllNumbers(group[0]) ):
            if (DEBUG):
                print("Found numbers crammed together "+str(group))
            groups[i].append(True)
            newGroup = []
            newGroup.append([])
            newGroup[0] = group[0][4:8]
            group[0] = group[0][0:4]
            boxes = SplitBoxHorizontal(group[1])
            newGroup.append(boxes[0] )
            group[1] = boxes[1]
            if (DEBUG):
                print("Split into "+str(group)+" and "+str(newGroup)+".")
            superGroups.append([Helpers.NumListToInt(group[0]), group[1] ])
            superGroups.append([Helpers.NumListToInt(newGroup[0]), newGroup[1] ])
        elif(not( IsAllNumbers(group[0]) )):
            if(group[0][0] == 24):
                #standalone &
                neighbors = FindLRNeighbors(groups, i)
                if( neighbors[0] != -1 and neighbors[1] != -1):
                    if (DEBUG):
                        print( "found & "+str(group)+" with neighbors " + str(groups[neighbors[0]])+ " and "+str(groups[neighbors[1]] ) )
                    uNeighbor = FindUpperNeighbor(groups, i)
                    if( uNeighbor >= 0):
                        if (DEBUG):
                            print( "Found upper neighbor as well "+str(groups[uNeighbor]))
                        box = Helpers.CombineBoxList([groups[neighbors[0]][1], groups[neighbors[1]][1], group[1], groups[uNeighbor][1] ])
                        outStr = str(Helpers.NumListToInt(groups[uNeighbor][0])) + " "
                        outStr += NumberIdentifier.GetChar(groups[neighbors[0]][0][0]) + "," + NumberIdentifier.GetChar(groups[neighbors[1]][0][0])                        
                        superGroups.append([outStr, box])
                        groups[i].append(True)
                        groups[uNeighbor].append(True)
                        groups[neighbors[0]].append(True)
                        groups[neighbors[1]].append(True)
            if( len(group[0]) == 1 ):
                continue
            elif( group[0][0] == 17 and group[0][1] == 16):
                # a to b
                neighbors = FindLRNeighbors(groups, i)
                if( neighbors[0] != -1 and neighbors[1] != -1):
                    if (DEBUG):
                        print( "found TO "+str(group)+" with neighbors " + str(groups[neighbors[0]])+ " and "+str(groups[neighbors[1]] ) )
                    uNeighbor = FindUpperNeighbor(groups, i)
                    if( uNeighbor >= 0):
                        if (DEBUG):
                            print( "Found upper neighbor as well "+str(groups[uNeighbor]))
                        box = Helpers.CombineBoxList([groups[neighbors[0]][1], groups[neighbors[1]][1], group[1], groups[uNeighbor][1] ])
                        outStr = str(Helpers.NumListToInt(groups[uNeighbor][0])) + " "
                        outStr += LettersInRangeToString(groups[neighbors[0]][0][0], groups[neighbors[1]][0][0])
                        superGroups.append([outStr, box])
                        groups[i].append(True)
                        groups[uNeighbor].append(True)
                        groups[neighbors[0]].append(True)
                        groups[neighbors[1]].append(True)
                else:
                    if (DEBUG):
                        print( "found TO "+str(group)+" but failed to find neighbors ")
            elif( group[0][1] == 24):
                # a&b
                uNeighbor = FindUpperNeighbor(groups, i)
                if( uNeighbor >= 0):
                    outStr = str(Helpers.NumListToInt(groups[uNeighbor][0])) + " "
                    outStr += NumberIdentifier.GetChar(group[0][0]) + "," + NumberIdentifier.GetChar(group[0][2])
                    if (DEBUG):
                        print( "Found & "+str(group)+" with upper neighbor "+str(groups[uNeighbor]))
                    box = Helpers.CombineBoxes(group[1], groups[uNeighbor][1])
                    superGroups.append([outStr, box])
                    groups[i].append(True)
                    groups[uNeighbor].append(True) 
            elif( 23 in group[0]):
                # a - b or num - num or num ltr - ltr
                if( group[0][0] > 9 and group[0][0] < 18):
                    # a - b
                    neighbor = FindUpperNeighbor(groups, i)
                    if( neighbor < 0 ):
                        neighbors = FindLRNeighbors(groups, i)
                        neighbor = max( neighbors[0], neighbors[1] )
                    if( neighbor >= 0):
                        outStr = str(Helpers.NumListToInt(groups[neighbor][0])) + " "
                        outStr += LettersInRangeToString(group[0][0], group[0][2])
                        if (DEBUG):
                            print( "Found - range "+str(group)+" with upper neighbor "+str(groups[neighbor]))
                        box = Helpers.CombineBoxes(group[1], groups[neighbor][1])
                        superGroups.append([outStr, box])
                        groups[i].append(True)
                        groups[neighbor].append(True)
                else: 
                    for index in range(len(group[0])):
                        if( group[0][index] == 23):
                            # number-number.. break
                            num1 = Helpers.NumListToInt(group[0][:index])
                            num2 = Helpers.NumListToInt(group[0][index+1:])
                            for num in range( num1, num2 + 1):
                                superGroups.append( [str(num), group[1]])
                            # outStr = GetNumRangeString(num1,num2)
                            # superGroups.append( [outStr, group[1]])
                            groups[i].append(True)
                            break
                        elif( group[0][index] > 9 ):
                            number = Helpers.NumListToInt(group[0][:index])
                            outStr = str(number) + " "
                            outStr += LettersInRangeToString(group[0][index], group[0][index+2])
                            superGroups.append( [outStr, group[1]])
                            groups[i].append(True)
                            break
            else:
                if( group[0][len(group[0])-1]>9 and IsAllNumbers(group[0][:len(group[0])-1])):                    
                    #number followed by a letter  1923B
                    outStr = str(Helpers.NumListToInt(group[0][:len(group[0])-1])) + " "
                    outStr += NumberIdentifier.GetChar(group[0][len(group[0])-1])
                    superGroups.append([outStr, group[1]])
                    groups[i].append(True)
                
    #find Stand-alone numbers not included in ranges
    for i in range(len(groups)):
        group = groups[i]
        if( len(group) == 2 and not IsNumber( group[0][0]) and (group[0][0] < 16 or (group[0][0] > 17 and group[0][0]<20 ))  ):
            #is a remaining letter first group between a and f
            neighbors = FindLRNeighbors(groups, i)
            if( not IsAllNumbers( groups[neighbors[0]][0] )):
                neighbors[0] = -1
            if( not IsAllNumbers( groups[neighbors[1]][0] )):
                neighbors[1] = -1
            neighbor = max( neighbors[0], neighbors[1])
            if( neighbor < 0):
                uNeighbor = FindUpperNeighbor( groups, i, 45)
                if( not IsAllNumbers( groups[uNeighbor][0] )):
                    uNeighbor = -1
                lNeighbor = FindLowerNeighbor( groups, i, 45)
                if( not IsAllNumbers( groups[lNeighbor][0] )):
                    lNeighbor = -1
                if( uNeighbor >=0 and lNeighbor >=0):
                    if( Helpers.GetBoxBoxDistY(group[1], groups[uNeighbor][1]) < Helpers.GetBoxBoxDistY(group[1], groups[lNeighbor][1]) ):
                        neighbor = lNeighbor
                    else:
                        neighbor = uNeighbor
                elif( uNeighbor >= 0):
                    neighbor = uNeighbor
                elif( lNeighbor >=0 ):
                    neighbor = lNeighbor
            if( neighbor >= 0):
                box = Helpers.CombineBoxes(group[1], groups[neighbor][1] )
                outStr = str(Helpers.NumListToInt(groups[neighbor][0])) + " "
                outStr += NumberIdentifier.GetChar(group[0][0])
                for index in range(1, len(group[0])):
                    outStr+= ","+NumberIdentifier.GetChar(group[0][index])
                superGroups.append([outStr, box])
                groups[i].append(True)
                groups[neighbor].append(True)
                continue
            



    plainNumbers = []
    others = []

    for index in range(len(groups)):
        if( len(groups[index]) < 3):
            if(IsAllNumbers( groups[index][0] )):
                plainNumbers.append([str(Helpers.NumListToInt(groups[index][0])), groups[index][1] ])
            else:
                others.append(["unknown."+str(index)+"_"+str(groups[index][1]),PadBox(groups[index][1], 20)])


                        


    return (superGroups, plainNumbers, others)

            

def testIntersect():
    testGroup = []   
    testGroup.append( [[], (1,0,3,0)] )
    testGroup.append( [[], (2,0,4,0)])
    testGroup.append( [[], (0,0,2,0)])
    testGroup.append( [[], (0,0,4,0)])
    testGroup.append( [[], (2,0,2,0)])
    testGroup.append( [[], (0,0,0,0)])
    testGroup.append( [[], (4,0,6,0)])

    def DoTest(list, ind1, ind2):
        print( "group "+str(list[ind1]) + "intersects " + str(list[ind2])+"?"+ str(DoGroupsOverlapX(list[ind1], list[ind2])))


    DoTest(testGroup, 0,1)
    DoTest(testGroup, 0,2)
    DoTest(testGroup, 0,3)
    DoTest(testGroup, 0,4)
    DoTest(testGroup, 0,5)
    DoTest(testGroup, 0,6)
import csv
import datetime
import os

def WriteMapLocations(dirname, mapName, mapLocations):
    outFile = dirname+"/"+mapName+".csv"
    print("Writing map data to "+outFile)
    with open(outFile, 'w') as csvfile:
        csvWriter = csv.writer(csvfile, delimiter=',',
                                quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csvWriter.writerow(['Place name', 'Addresses', 'Latitude-longitude information'])
        for location in mapLocations:
            csvWriter.writerow([str(location[0][0]).lower(), str(location[0][0]).lower(), str(location[1][0])+", "+str(location[1][1])])
import datetime
import os

class FolderManager:
    def __init__(self, path = "."):
        self.path = path
        if( not os.path.exists( path )):
            os.mkdir(path)

    def CreateFolder(self, folderName = ""):
        pathName = self.path + "/" + folderName
        if( not os.path.exists( pathName )):
            index = pathName.find("/", 0)
            while( index > -1  and index < len(pathName) - 1 ):
                if( not os.path.exists(pathName[:index]) ):
                    os.mkdir(pathName[:index])
                index = pathName.find("/", index+1)
            os.mkdir(pathName)

    def GetPath( self, path = "", createIfAbsent = True):
        if( createIfAbsent):
            lastSlashIndex = path.find("/", 0)
            while( lastSlashIndex > -1):
                nextIndex = path.find("/", lastSlashIndex+1)
                if( nextIndex == -1): break
                lastSlashIndex = nextIndex
            if( lastSlashIndex > 0 ):
                self.CreateFolder(path[:lastSlashIndex])
        return self.path + "/"+ path

    def DoesFileExist(self, fileName):
        return os.path.exists( self.path + "/" + fileName)

def create_timestamp_folder(prefix ):
    date = datetime.datetime.now()
    dirname = date.strftime(prefix+"%Y_%m_%d.%H_%M_%S" )
    return FolderManager(dirname)

def create_folder(root):
    return FolderManager(root)

def CreateFolders(prefix = ""):
    date = datetime.datetime.now()
    dirname = date.strftime("%Y_%m_%d.%H_%M_%S" )
    if( prefix != ""): 
        dirname = prefix + dirname
    os.mkdir( dirname )
    return dirname

def AddFolder( root, name ):
    os.mkdir( root + name)
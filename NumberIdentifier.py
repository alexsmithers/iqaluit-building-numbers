import tensorflow as tf
import numpy as np
#from tensorflow.keras.preprocessing import image

MAX_LEARNED_CHAR = 25

from PIL import Image
# ----------['0','1','2','3','4','5','6','7','8','9',10 ,11 ,12 ,13 ,14 ,15 ,16 ,17 ,18, 19 , 20, 21, 22    , 23, 24,25 ,26 ,27, 28  
valToChar = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','o','t','A','B','C','D','slash','-','&','g','h','i','j','other']


def load_model(modelPath = './ModelFiles/num_model_3.h5'):
    print( "Loading Identifier Model "+modelPath)
    return tf.keras.models.load_model(modelPath)

def GetWildcardIndex():
    return MAX_LEARNED_CHAR

def GetNumber(model, image):
    pix = np.array(image)*(1./255)
    pix = np.expand_dims(pix, axis = 0)
    pix = tf.image.rgb_to_grayscale(pix)
    prediction = model.predict(pix)
    highIndex = -1
    highVal = -1
    for index in range(MAX_LEARNED_CHAR + 1):
        if( prediction[0][index] > highVal):
            highIndex = index
            highVal = prediction[0][index]
    return ( highIndex, highVal )

def GetChar(num):
    return valToChar[num]



#from tensorflow.keras.preprocessing.image import ImageDataGenerator


# test_datagen = ImageDataGenerator(rescale=1./255)
# test_generator = test_datagen.flow_from_directory(
#         './Learning Data/Test/',  # This is the source directory for training images
#         target_size=(16, 16),  # All images will be resized to 150x150
#         batch_size=128,
#         # Since we use binary_crossentropy loss, we need binary labels
#         class_mode='sparse')

# new_model.evaluate(test_generator)



# predictions = new_model.predict(test_generator)

# print( test_generator)



# img_width, img_height = 16, 16
# img = image.load_img("./TestImages/TestImg4.png", target_size = (img_width, img_height))
# img = image.img_to_array(img)
# img = np.expand_dims(img, axis = 0)

# prediction = new_model.predict(img)

# print(prediction)

# im = Image.open("./TestImages/TestImg4.png")
# print( GetNumber(im) )

# pix = np.array(im)*(1./255)
# pix = np.expand_dims(pix, axis = 0)

# prediction = new_model.predict(pix)
# print(prediction)

# dataset = tf.data.Dataset.from_tensors( pix ) 

# prediction = new_model.predict( dataset )

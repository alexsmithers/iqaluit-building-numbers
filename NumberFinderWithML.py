
import FolderManager
import os
import sys
import ImageParseHelpers as Helpers
import NumberIdentifier
from PIL import Image
import GroupJoiner

import pickle

debug = False # Print full debug output
searchDist = 15 #When a character pixel is found, how far around it do we search for a letter
maxDist = [5, 4] # How far apart can numbers be to be considered part of the same number?
OUTPUT_CHARACTERS = True # Write all found characters to a directory
OUTPUT_NUMBERS = True # Write all found numbers to a directory
OUTPUT_SUPER_GROUPS = True
USER_IDENTIFY = False # When there are numbers we're unsure of, let the user specify

FOUND_NUMBERS_FILE = "Data/foundNumbers.dat"
IDENTIFIED_NUMBERS_FILE = "Data/identifiedNumbers.dat"
GROUPED_NUMBERS_FILE = "Data/groupedNumbers.dat"
SUPERGROUPED_NUMBERS_FILE = "Data/superGroupedNumbers.dat"

def CreateFolders(dirname):
    if( OUTPUT_CHARACTERS):
        os.mkdir( dirname + "/Characters" )
        for index in range(NumberIdentifier.GetWildcardIndex() + 1):
            os.mkdir( dirname + "/Characters/"+str(index) )
        os.mkdir( dirname + "/Characters_Unsure" )
    if( OUTPUT_NUMBERS):
        os.mkdir( dirname + "/Numbers" )

def load_data(datFile):
    with open(datFile, 'rb') as fpkl:
        data = pickle.load(fpkl)
        fpkl.close()
        return data

def save_structure(data, name):
    with open(name, 'wb') as fpkl:
        pickle.dump(data, fpkl)
        fpkl.close()

def GenerateFoundNumbersArray(im):
    print("Generating Found Numbers Array")
    rgbIm = im.convert('RGB')
    
    foundNumbers = []
    exploredMatrix = []
    for yIndex in range(im.size[1]):
        exploredMatrix.append([False]*im.size[0])

    exploredCount = 0
    printFrequency = .01 # print update every 1 pct
    lastPrint = 0
    totalPix = im.size[0] * im.size[1]
    for i in range( im.size[0] ):
        for j in range ( im.size[1] ):
            if exploredMatrix[j][i]:
                continue
            exploredMatrix[j][i] = True
            exploredCount += 1
            thisPixel = rgbIm.getpixel((i,j))
            if thisPixel == (76,76,76) and Helpers.validatePixel(rgbIm, i,j) :
                if(debug):
                    print("Found match at ("+str(i)+","+str(j)+")")
                topLeft = [max(0, i-searchDist),max(0, j-searchDist)] 
                
                box = ( topLeft[0], topLeft[1], min(im.size[0], i + searchDist), min(im.size[0], j + searchDist)  )
                region = im.crop(box)
                newPixelX = searchDist if i > searchDist else i
                newPixelY = searchDist if j > searchDist else j
                matrix = Helpers.exploreNumber(region, newPixelX, newPixelY)
                
                if( debug ):
                    print("Exploration result:")
                    for row in matrix:
                        print( row )
                
                numPixelsExplored = Helpers.superimposeSearchMatrixOnExplored(exploredMatrix, matrix, topLeft[0], topLeft[1])
                exploredCount += numPixelsExplored

                if Helpers.isNumberWithinBounds(matrix ):
                    newBox = Helpers.IsolateNumber(matrix, topLeft)
                    newBox = (max(0, newBox[0] - 1), max(0, newBox[1] - 1), min( newBox[2] + 1, im.size[0]), min( newBox[3] + 1, im.size[1]))
                    boxCenter = Helpers.GetBoxCenter(newBox)
                    if( debug): print( "newbox 0 - " + str(newBox[0]) )
                    Helpers.AddToCharArray(foundNumbers, boxCenter, newBox)
            if( exploredCount - lastPrint > totalPix * printFrequency ):
                lastPrint = exploredCount
                sys.stdout.write( "\rExplored pixels %d of %d (%d pct). Found Chars: %d" % ( exploredCount, totalPix , int(100* exploredCount/totalPix ), len(foundNumbers)))
    sys.stdout.write( "Complete!\n")
    return foundNumbers

def is_wanted_number( wantedNumbers, num):
    if( len(wantedNumbers) == 0): return True
    return num in wantedNumbers

def identify_numbers(foundNumbers, model, im, folderManager, wantedNumbers = [] ):
    print("Identifying Numbers")
    numIdentified = 0
    for index in range(len(foundNumbers)):
        sys.stdout.write( "\rIdentifying number %d of %d (%d pct)." %( numIdentified, len(foundNumbers), int(100*numIdentified/ len(foundNumbers))))
        numRegion = im.crop(foundNumbers[index][1])
        newImage = Helpers.PasteRegionIntoNewImage(numRegion)
        MLNum = NumberIdentifier.GetNumber(model, newImage)

        lowProb = False
        if( MLNum[1] < 0.7 ):
            lowProb = True
            if( USER_IDENTIFY):
                newImage.show()
                userNum = -1
                while( userNum < 0 or userNum > NumberIdentifier.GetWildcardIndex() ):
                    userNum = int(input(  "What is this number? Suspected: "+str(MLNum[0])+" with probability: "+str(MLNum[1]) ) )
                MLNum = ( int(userNum), 1.0)

        if( is_wanted_number(wantedNumbers, MLNum[0] ) ):
            foundNumbers[index].append(MLNum)

            if(OUTPUT_CHARACTERS):
                if( lowProb):
                    newImage.save( folderManager.GetPath("Characters_Unsure/img_"+str(index)+"-"+NumberIdentifier.GetChar(MLNum[0])+"_"+str(MLNum[1])+".png") )
                else:
                    newImage.save( folderManager.GetPath( "Characters/"+str(MLNum[0])+"/img_"+str(index)+"-"+NumberIdentifier.GetChar(MLNum[0])+"_"+str(MLNum[1])+".png"))
        numIdentified+=1
    sys.stdout.write( "\n")


def get_number_string(number):
    outStr = ""
    for digit in number:
        outStr += NumberIdentifier.GetChar(digit)
    return outStr

def outputGroupedNumbers(groupedNumbers, folderManager, im):
    for index in range(len(groupedNumbers)):
        number = groupedNumbers[index]
        numberBox = number[1]
        numRegion = im.crop(numberBox)
        outStr = get_number_string(number[0])
        outName = folderManager.GetPath("/Numbers/img"+str(index)+"_"+outStr+".png")
        numRegion.save( outName)

def group_numbers(foundNumbers, im, folderManager):
    groupedNumbers = Helpers.GroupNumbers(foundNumbers, maxDist)
    if( OUTPUT_NUMBERS):
        outputGroupedNumbers(groupedNumbers, folderManager, im)
    return groupedNumbers



def write_group_to_disk(group, im, dir):
    for index in range(len(group)):
        region = im.crop(group[index][1])
        region.save( dir+"/"+str(index)+"_"+str(group[index][0])+".png")

def write_super_groups_to_disk(superGroups, im, folderManager):
    write_group_to_disk(superGroups[0], im, folderManager.GetPath("/SuperGroups/"))
    write_group_to_disk(superGroups[1], im, folderManager.GetPath("/FlatNums/"))
    write_group_to_disk(superGroups[2], im, folderManager.GetPath("/Others/"))
    print("SuperGroupResults: FlatNums: "+str(len(superGroups[1]))+" SuperGroups: "+str(len(superGroups[0]))+ " others: "+str(len(superGroups[2])))

def form_super_groups(groupData, imageFile, dirname):
    groups = GroupJoiner.SuperGroupNumbers(groupData)
    write_super_groups_to_disk(groups, imageFile, dirname)
    return groups


# Broken! Do not use until replacing with fileManager
def rebuild_full(fileName, model, dirname):
    im = Image.open(fileName)
    data = GenerateFoundNumbersArray(im)
    save_structure(data, dirname+"/foundNumbers.dat")
    identify_numbers(data, model, im, dirname)
    save_structure(data, dirname+"/identifiedNumbers.dat")
    groupedNums = group_numbers( data, im, dirname)
    save_structure(groupedNums, dirname+"/groupedNumbers.dat")

    file = open( dirname+'/groupResults.txt','w') 
    for line in groupedNums:
            file.write(str(line))
    file.close()

def get_found_numbers(im, folderManager):
    if( folderManager.DoesFileExist(FOUND_NUMBERS_FILE )):
        return load_data(folderManager.GetPath(FOUND_NUMBERS_FILE))
    else:
        data = GenerateFoundNumbersArray(im)
        save_structure(data, folderManager.GetPath(FOUND_NUMBERS_FILE))
        return data

def get_identified_numbers_new( im, folderManager, model):
    if( folderManager.DoesFileExist(IDENTIFIED_NUMBERS_FILE )):
        return load_data(folderManager.GetPath(IDENTIFIED_NUMBERS_FILE))
    else:
        foundNumbers = get_found_numbers(im, folderManager)
        identify_numbers(foundNumbers, model, im, folderManager)
        save_structure(foundNumbers, folderManager.GetPath(IDENTIFIED_NUMBERS_FILE))
        return foundNumbers

def get_grouped_numbers( im, folderManager, model):
    if( folderManager.DoesFileExist(GROUPED_NUMBERS_FILE )):
        return load_data(folderManager.GetPath(GROUPED_NUMBERS_FILE))
    else:
        identifiedNumbers = get_identified_numbers_new(im, folderManager, model)
        data = group_numbers(identifiedNumbers, im, folderManager)
        save_structure(data, folderManager.GetPath(GROUPED_NUMBERS_FILE))
        return data

def get_super_groups( im, folderManager, model):
    if( folderManager.DoesFileExist(SUPERGROUPED_NUMBERS_FILE )):
        return load_data(folderManager.GetPath(SUPERGROUPED_NUMBERS_FILE))
    else:
        groupedNumbers = get_grouped_numbers(im, folderManager, model)
        data = GroupJoiner.SuperGroupNumbers(groupedNumbers)
        if(OUTPUT_SUPER_GROUPS):
            write_super_groups_to_disk(data, im, folderManager)
        outData = data[0] + data[1]
        save_structure(outData, folderManager.GetPath(SUPERGROUPED_NUMBERS_FILE))
        return outData

def get_super_groups_iqaluit( im, folderManager, model):
    specialCases = [
        [[25],(5470, 4079, 5480, 4092)],
        [[29],(6326, 4562, 6332, 4580)],
        [[25],(5954, 4578, 5964, 4592)],
    ]

    def AugmentGroupData(groups, newGroup):
        for index in range(len(groups)):
            if(( groups[index][1][1] == newGroup[1][1] and groups[index][1][0] > newGroup[1][0]) or groups[index][1][1] > newGroup[1][1] ):
                groups.insert(index, newGroup)
                return
        groups.append(newGroup)

    def AddSpecialCases(groups):
        for specialCase in specialCases:
            AugmentGroupData( groups, specialCase )
        return groups

    def FixKnownErrors(groups):
        groups[1256][0] = [4,8,4]

    if( folderManager.DoesFileExist(SUPERGROUPED_NUMBERS_FILE )):
        return load_data(folderManager.GetPath(SUPERGROUPED_NUMBERS_FILE))
    else:
        print("Building Iqaluit SuperGroups with data fixing.")
        data = get_grouped_numbers(im, folderManager, model)
        newData = []
        for i in range(len(data)):
            if( i == 455): continue
            newData.append(data[i])
        
        groups = AddSpecialCases(newData)
        FixKnownErrors(groups)

        data = GroupJoiner.SuperGroupNumbers(groups)
        if(OUTPUT_SUPER_GROUPS):
            write_super_groups_to_disk(data, im, folderManager)
        outData = data[0] + data[1]
        save_structure(outData, folderManager.GetPath(SUPERGROUPED_NUMBERS_FILE))
        return outData

def process_map_numbers(imgFileName, folderManager, model):
    im = Image.open(imgFileName)
    superGroups =  get_super_groups( im, folderManager, model)
    
    return superGroups


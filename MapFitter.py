import numpy as np
import NumberFinderWithML as finder
import ImageParseHelpers as Helpers
import csvExport

# mapInfo_small = ( "SmallTest", "NumbersSmall.png", ([[64,106],[ 63.729323, -68.445227]], [[398,246],[ 63.728715, -68.441922]] ))

def swap_long_lat(vec):
    outVec = [vec[1], vec[0]]
    return outVec

#returns a tuple of scale and map coordinates of 0,0
def get_map_keys(mapDimensionInfo):
    mapLocation1 = np.array(mapDimensionInfo[0])
    mapLocation2 = np.array(mapDimensionInfo[1])
    mapLocation1[1] = swap_long_lat(mapLocation1[1])
    mapLocation2[1] = swap_long_lat(mapLocation2[1])
    scale  = (mapLocation2[1] - mapLocation1[1])/(mapLocation2[0] - mapLocation1[0])
    zeroLocation = swap_long_lat(mapLocation1[1] + (np.array([0,0]) - mapLocation1[0])*scale)
    return( scale, zeroLocation)

def get_geo_location(mapKeys, mapPos):
    toTestLocation = (mapPos)*mapKeys[0]
    testMapPos = toTestLocation + swap_long_lat(mapKeys[1])
    return swap_long_lat(testMapPos)

def plot_groups_on_map(mapInfo, groupedNumbers):
    print( "Plotting groups on Map "+mapInfo[1])
    mapKeys = get_map_keys(mapInfo[2])
    fitResults = []
    for number in groupedNumbers:
        center = Helpers.GetBoxCenter(number[1])
        geoLocation = get_geo_location( mapKeys, center)
        fitResults.append( (number, geoLocation))
    return fitResults

def export_numbers(mapInfo, folderManager, groupInfo):
    plotData = plot_groups_on_map(mapInfo, groupInfo)
    csvExport.WriteMapLocations( folderManager.GetPath(), mapInfo[0], plotData )
from PIL import Image
import NumberIdentifier

darkThreshold = 129

adjacencyList = [[-1,0],[1,0],[0,-1],[0,1]]

numbersOnly = True

def validatePixel(image, x, y):
    for x2 in range (max(0, x-3), min(image.size[0], x + 3) ):
        for y2 in range (max(0, y-3), min(image.size[1], y + 3) ):
            if image.getpixel((x2,y2)) == (255, 255, 255):
                #print("Found match at pixel {} {} ".format( x2, y2))
                return True
    return False

def isDark(pixel):
    return ((pixel[0] + pixel[1] + pixel[2]) / 3 ) < darkThreshold
    
def isWithinBounds(visited, x, y):
    return( x >= 0 and x < len(visited[0]) and y >= 0 and y <len(visited) )

def isVisited( visited, x, y):
    return visited[y][x] != -1

def shouldExplore(visited, image, x, y):
    #withinBounds = isWithinBounds(visited, x, y)

    #visited = isVisited(visited, x, y)
    return isWithinBounds(visited, x, y) and not isVisited(visited, x, y)  

def exploreNumberRecursive( visited, imageData, x, y ):
    for offset in adjacencyList:
        nextX = x + offset[0]
        nextY = y + offset[1]
        if( shouldExplore(visited, imageData, nextX, nextY )):
            boolDark = isDark(imageData[nextX,nextY])
            visited[nextY][nextX] = boolDark
            if( boolDark ):
                exploreNumberRecursive( visited, imageData, nextX, nextY)

def exploreNumber( image, x, y):
    im = image.load()
    visited = []
    for yIndex in range(image.size[1]):
        visited.append([-1]*image.size[0])
    visited[y][x] = isDark(im[x,y])
    exploreNumberRecursive( visited, im, x, y )
    return visited

def isNumberWithinBounds(matrix):
    for index in range(len(matrix)):
        if( index == 0 or index == len(matrix)-1):
            for index2 in range(len(matrix[0])):
                if( matrix[index][index2] == True ):
                    return False
        else:
            if( matrix[index][0] == True or matrix[index][len(matrix[0])-1]==True):
                return False
    return True

# x, y should be the top left corner of search, on explored
def superimposeSearchMatrixOnExplored(explored, search, x, y):
    exploredCount = 0
    for xIndex in range(len(search[0])):
        for yIndex in range (len(search) ):
            if( search[yIndex][xIndex] != -1):
                if( not(explored[y+yIndex][x+xIndex]) ):
                    exploredCount+=1
                    explored[y+yIndex][x+xIndex] = True
    return exploredCount

def IsolateNumber(matrix, topLeft):
    xBounds = [ len(matrix[0]), -1]
    yBounds = [ len(matrix), -1]
    for xIndex in range( len(matrix[0])):
        for yIndex in range( len(matrix)):
            if matrix[yIndex][xIndex] == True:
                if yIndex >  yBounds[1]:
                    yBounds[1] = yIndex
                if yIndex < yBounds[0]:
                    yBounds[0] = yIndex
                if xIndex >  xBounds[1]:
                    xBounds[1] = xIndex
                if xIndex < xBounds[0]:
                    xBounds[0] = xIndex
    return (topLeft[0] + xBounds[0], topLeft[1] +yBounds[0],topLeft[0] + xBounds[1], topLeft[1] + yBounds[1])

#Add any found number to charArray such that it's sorted by y then x
def AddToCharArray(charArray, pos, box):
    for index in range(len(charArray)):
        if(( charArray[index][0][1] == pos[1] and charArray[index][0][0] > pos[0]) or charArray[index][0][1] > pos[1] ):
            charArray.insert( index, [pos, box])
            return
    charArray.append([pos, box])

def CombineBoxes( box1, box2):
    return (min(box1[0], box2[0]), min(box1[1], box2[1]), max(box1[2], box2[2]), max(box1[3], box2[3]))

def CombineBoxList( list ):
    box = list[0]
    for index in range( 1, len( list) ):
        box = CombineBoxes(list[index], box)
    return box

def GetBoxPosDist( box, pos):
    outDist = [0,0]
    if( pos[0] < box[0]):
        outDist[0] = box[0] - pos[0]
    elif( pos[0] > box[2]):
        outDist[0] = pos[0] - box[2]
    else:
        outDist[0] = min( pos[0]-box[0], box[2]-pos[0] )
    if( pos[1] < box[1]):
        outDist[1] = box[1] - pos[1]
    elif( pos[1] > box[3]):
        outDist[1] = pos[1] - box[3]
    else:
        outDist[0] = min( pos[1]-box[1], box[3]-pos[1] )
    return outDist

def GetBoxPosDistX(box, pos):
    if( pos[0] < box[0]):
        return box[0] - pos[0]
    elif( pos[0] > box[2]):
        return pos[0] - box[2]
    else:
        return 0
    
def GetBoxBoxDistX(box1, box2):
    b1Center = GetBoxCenter(box1)
    b2Center = GetBoxCenter(box2)
    b1Width = box1[2] - box1[0]
    b2Width = box2[2] - box2[0]
    return abs( b2Center[0] - b1Center[0]) - b1Width/2. - b2Width / 2.

def GetBoxBoxDistY(box1, box2):
    b1Center = GetBoxCenter(box1)
    b2Center = GetBoxCenter(box2)
    b1Height = box1[3] - box1[1]
    b2Height = box2[3] - box2[1]
    return abs( b2Center[1] - b1Center[1]) - b1Height/2. - b2Height / 2.

def GetBoxCenterPosDistY(box, pos):
    return abs(GetBoxCenter(box)[1] - pos[1])

def FindNextNeighbor(charArray, startIndex, neighboredChars, curBox, maxDist ):
    for index in range(startIndex+1, len(charArray)):
        if( index in neighboredChars): continue
        boxDist = [GetBoxBoxDistX( curBox, charArray[index][1]), GetBoxCenterPosDistY(curBox, charArray[index][0]) ]
        if( boxDist[1] > maxDist[1]): return -1
        if( boxDist[0] + boxDist[1] > maxDist[0]): continue
        return index
    return -1

def GetNumber( charArray, numIndices ):
    i=len(numIndices)-1
    outNum=[]
    
    for index in numIndices :
        charNum = charArray[index][2][0]
        if( charNum == NumberIdentifier.GetWildcardIndex() ): return -1
        outNum.append(charNum)
        i-=1
    return outNum

def GroupNumbers( charArray, maxDist ):
    print("Grouping Numbers")
    neighboredChars = []
    numberGroups = []
    for charIndex in range(len(charArray)):
        if( charIndex in neighboredChars): continue
        indexList = [charIndex]
        neighboredChars.append(charIndex)
        curBox = charArray[charIndex][1]
        nextNeighbor = FindNextNeighbor(charArray, charIndex, neighboredChars, curBox, maxDist)
        while( nextNeighbor != -1):
            for index in range(len(indexList)):
                if( charArray[nextNeighbor][0][0] < charArray[indexList[index]][0][0]):
                    indexList.insert(index, nextNeighbor )
                    break
                if( index == len(indexList) - 1):
                    indexList.append(nextNeighbor)
                    break
            neighboredChars.append(nextNeighbor)
            curBox = CombineBoxes(curBox, charArray[nextNeighbor][1])
            nextNeighbor =FindNextNeighbor(charArray, charIndex, neighboredChars, curBox, maxDist)
        number= GetNumber(charArray, indexList)
        if( not(numbersOnly) or number != -1 ):
            numberGroups.append( [number, curBox] )

    return numberGroups

def GetBoxCenter(box):
    return [ (box[0] + box[2])/2, (box[1]+box[3])/2]

def PasteRegionIntoNewImage( region):
    img = Image.new('RGB', (16,16), (255, 255, 255))
    img.paste( region, ((16-region.size[0])//2, (16-region.size[1])//2 ))
    return img

def NumListToInt(list):
    outNum = 0
    for index in range(len(list)):
        outNum += list[index] * (10**( len(list) - index - 1 ))
    return outNum
